/*
 * number_stream_functions.cpp
 *
 *  Created on: 17 марта 2015
 *      Author: strangeman
 */

#include "number_stream_functions.hpp"

bool IgnoreUntilEOL ( std::istream & _stream ) {
	bool isFoundSomethingElse = false;
	while (true) {
		if ( _stream.eof() ) {
			break;
		}

		if ( _stream.get() == '\n' ) {
			break;
		}

		isFoundSomethingElse = true;
	}
	return isFoundSomethingElse;
}

void IgnoreNumbers ( std::istream & _stream ) {
	char temp;
	do {
		if (_stream.eof()) {
			break;
		}
		temp = _stream.get();
	}
	while ( ((temp >= '0') && (temp <= '9')) || ( temp == '-' ) || (temp == '.') );

	_stream.unget();
}

bool FoundNumberInLineAndEatEverythingElse ( std::istream & _stream ) {
	char temp;
	do {
		if ( _stream.eof() ) {
			return false;
		}

		temp = _stream.get();

		if ( (temp == '\n')  ) {
			return false;
		}

	}
	while ( ! (((temp >= '0') && (temp <= '9')) || ( temp == '-' )) );

	_stream.unget();
	return true;
}

//Пытается получить из потока следующий int, возвращает:
// -1, если число в строке не найдено;
// 1, если перед int встретились какие-либо символы;
// 0 - во всех остальных случаях.
int GetNextInt (int & _value, std::istream & _stream) {
	char temp = _stream.get();
	int status;
	if ( ! (((temp >= '0') && (temp <= '9')) || ( temp == '-' ) ) ) {
		if ( FoundNumberInLineAndEatEverythingElse ( _stream ) )
			status = 1;
		else
			return -1;
	}
	else {
		_stream.unget();
		status = 0;
	}

	_stream >> _value;
	IgnoreNumbers ( _stream );
	return status;
}

bool GetNextReal (double & _value, std::istream & _stream) {
	if ( FoundNumberInLineAndEatEverythingElse ( _stream ) ) {
		_stream >> _value;
		return true;
	}
	return false;
}

