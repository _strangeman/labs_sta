/*
 * number_stream_functions.hpp
 *
 *  Created on: 17 марта 2015
 *      Author: strangeman
 */

#ifndef NUMBER_STREAM_FUNCTIONS_HPP_
#define NUMBER_STREAM_FUNCTIONS_HPP_

#include <iostream>

void IgnoreNumbers ( std::istream & _stream );

bool IgnoreUntilEOL ( std::istream & _stream );

bool FoundNumberInLineAndEatEverythingElse (std::istream & _stream);

//Возвращает статус, записывает значение в _value
int GetNextInt (int & _value, std::istream & _stream);

bool GetNextReal (double & _value, std::istream & _stream);

#endif /* NUMBER_STREAM_FUNCTIONS_HPP_ */
