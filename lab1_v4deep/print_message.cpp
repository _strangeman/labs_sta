#include "print_message.hpp"

void PrintMessage ( MESSAGE_ID _message, std::ostream & _stream, int _fid, const int _lineNumber ) {
	const char * fileNaming;

	switch ( _fid ) {
	case ONE_OF:
		fileNaming = "one of";
		break;

	case FIRST_SOURCE_FILE:
		fileNaming = "the first source";
		break;

	case SECOND_SOURCE_FILE:
		fileNaming = "the second source";
		break;

	case RESULT_FILE:
		fileNaming = "the result file";
		break;

	default:
		assert( ! "Unknown FILE_ID" );
	}

	switch ( _message ) {
	case ERR_FILE_NON_SPECIFIED:
		_stream << "ERROR: at least " << fileNaming << " file was not specified, exiting.\n";
		break;

	case ERR_BAD_FILE:
		_stream << "ERROR: Unable to open " << fileNaming << " file, exiting.\n";
		break;

	case WARN_UNEXP_CHAR:
		_stream << "WARNING: Unexpected character before bank ID on line "
		<< _lineNumber << " in "<< fileNaming << " file. Trying to find ID in remaining symbols.\n";
		break;

	case WARN_UNEXP_OEL:
		_stream << "WARNING: Unexpected end of line number " << _lineNumber
		<< " in " << fileNaming << " file. Assuming payment amount for is 0\n";
		break;

	case WARN_UNEXP_CHARS_UNTIL_EOL:
		_stream << "WARNING: Unexpected character(s) in the end of line number " << _lineNumber
		<< " in "<< fileNaming << " file, ignoring.\n";
		break;

	case WARN_WRONG_ID:
		_stream << "WARNING: Wrong ID on line number " << _lineNumber
		<< " in "<< fileNaming << " file, ignoring.\n";
		break;

	case WARN_EMPTY_LINE:
		_stream << "WARNING: Line number " << _lineNumber
		<< " in "<< fileNaming << " is empty, ignoring.\n";
		break;
	}
}
