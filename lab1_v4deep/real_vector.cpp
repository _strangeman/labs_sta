#include "real_vector.hpp"

#include <cstring>
#include <iostream>
#include <cassert>


void RealVectorInit ( RealVector & _vector, int _allocatedSize )
{
    _vector.m_pData      = new double[ _allocatedSize ];
    _vector.m_nAllocated =  _allocatedSize;
    _vector.m_nUsed      = 0;
}


void RealVectorDestroy ( RealVector & _vector )
{
    delete[] _vector.m_pData;
}


void RealVectorClear ( RealVector & _vector )
{
    _vector.m_nUsed = 0;
}


bool RealVectorIsEmpty ( const RealVector & _vector )
{
    return _vector.m_nUsed == 0;
}


void RealVectorGrow ( RealVector & _vector )
{
    int nAllocatedNew = _vector.m_nAllocated * 2;
    double * pNewData = new double[ nAllocatedNew ];

    memcpy( pNewData, _vector.m_pData, sizeof( double ) * _vector.m_nAllocated );

    delete[] _vector.m_pData;
    _vector.m_pData = pNewData;

    _vector.m_nAllocated = nAllocatedNew;
}


void RealVectorPushBack ( RealVector & _vector, double _data )
{
    if ( _vector.m_nUsed == _vector.m_nAllocated )
        RealVectorGrow( _vector );

    _vector.m_pData[ _vector.m_nUsed++ ] = _data;
}



void RealVectorPopBack ( RealVector & _vector )
{
    assert( ! RealVectorIsEmpty( _vector ) );

    -- _vector.m_nUsed;
}


void RealVectorRead ( RealVector & _vector, std::istream & _stream )
{
    while ( true )
    {
        double temp;
        _stream >> temp;
        if ( _stream )
            RealVectorPushBack( _vector, temp );
        else
            break;
    }    
}


void RealVectorReadTillZero ( RealVector & _vector, std::istream & _stream )
{
    while ( true )
    {
        double temp;
        _stream >> temp;
        if ( _stream && temp != 0 )
            RealVectorPushBack( _vector, temp );
        else
            break;
    }
}


void RealVectorPrint ( const RealVector & _vector, std::ostream & _stream, char _sep )
{
    for ( int i = 0; i < _vector.m_nUsed; i++ )
        _stream << _vector.m_pData[ i ] << _sep;
}


void RealVectorInsertAt ( RealVector & _vector, int _position, double _data )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    int newUsed = _vector.m_nUsed + 1;
    if ( newUsed > _vector.m_nAllocated )
        RealVectorGrow( _vector );

    for ( int i = _vector.m_nUsed; i > _position; i-- )
        _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

    _vector.m_pData[ _position ] = _data;

    _vector.m_nUsed = newUsed;
}


void RealVectorDeleteAt ( RealVector & _vector, int _position )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
        _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

    -- _vector.m_nUsed;
}
