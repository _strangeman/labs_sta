#ifndef _REAL_VECTOR_HPP_
#define _REAL_VECTOR_HPP_

#include <iostream>


struct RealVector
{
    double * m_pData;
    int m_nUsed;
    int m_nAllocated;
};


void RealVectorInit ( RealVector & _vector, int _allocatedSize = 10 );

void RealVectorDestroy ( RealVector & _vector );

void RealVectorClear ( RealVector & _vector );

bool RealVectorIsEmpty ( const RealVector & _vector );

void RealVectorPushBack ( RealVector & _vector, double _data );

void RealVectorPopBack ( RealVector & _vector );

void RealVectorInsertAt ( RealVector & _vector, int _position, double _data );

void RealVectorDeleteAt ( RealVector & _vector, int _position );

void RealVectorRead ( RealVector & _vector, std::istream & _stream );

void RealVectorReadTillZero ( RealVector & _vector, std::istream & _stream );

void RealVectorPrint ( const RealVector & _vector, std::ostream & _stream, char _sep = ' ' );


#endif //  _REAL_VECTOR_HPP_
