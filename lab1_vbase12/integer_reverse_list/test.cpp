#include "integer_reverse_list.hpp"
#include <cassert>

int main() {

    // создаем и инициализируем список
    IntegerReverseList l;
    IntegerReverseListInit( l );

    // добавляем данные в список
    IntegerReverseListPushBack( l, 1 );
    IntegerReverseListPushBack( l, 2 );
    IntegerReverseListPushBack( l, 3 );

    // проверяем расположение данных в списке
    assert( l.m_pFirst->m_value == 1 );
    assert( l.m_pLast->m_value == 3 );
    assert( l.m_pLast->m_pPrev->m_value == 2 );

    // тестируем вывод
    IntegerReverseListPrint(l, std::cout);

    // очищаем список
    IntegerReverseListClear( l );

    std::cout << "\nInput new list elements: ";

    // тестируем ввод
    IntegerReverseListReadTillZero(l, std::cin);

    std::cout << "\nNow list contains: ";
    IntegerReverseListPrint(l, std::cout);

    // тестируем удаление последнего
    IntegerReverseListPopBack( l );

    // и первого элемента
    IntegerReverseListPopFront( l );

    std::cout << "\nFirst and last elements was deleted. Now list contains: ";
    IntegerReverseListPrint(l, std::cout);

    // уничтожаем список
    IntegerReverseListDestroy( l );

	return 0;
}
