/*
 * handle_visit_data.hpp
 *
 *  Created on: 7 мая 2015
 *      Author: strangeman
 */

#ifndef HANDLE_VISIT_DATA_HPP_
#define HANDLE_VISIT_DATA_HPP_

#include "time_range_multimap.hpp"
#include "time_range_vector.hpp"
#include "time_range.hpp"

void PushNewVisitToMap (TimeRangeMap & _map, const TimeRange & _visitTime);

void FillRandomVisits (TimeRangeMap & _map, int _nOfVisits);

int GetPeriodIDWithMaxAttendance (TimeRangeMap & _map);

void PrintVisitDataByPeriod (const TimeRangeMap & _map, const int _idOfPeriod, std::ostream & _stream, const char _sep = '-');

void PrintVisitDataByAllPeriods (const TimeRangeMap & _map, std::ostream & _stream, const char _sep = '-');

void PrintVisitDataByMaxAttandance (const TimeRangeMap & _map, std::ostream & _stream, const char _sep = '-');

#endif /* HANDLE_VISIT_DATA_HPP_ */
