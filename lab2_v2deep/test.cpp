/*
 * test.cpp
 *
 *  Created on: 6 мая 2015
 *      Author: strangeman
 */

#include "time_range_multimap.hpp"
#include "time_range.hpp"
#include "handle_visit_data.hpp"
#include <ctime>
#include <iostream>
#include <fstream>

int main (int _argc, char** _argv) {
	int numberOfVisits = 50;
	int numOfPeriodsInDay = 48;
	TimeRangeMap * pMap = TimeRangeMapCreate (numOfPeriodsInDay);
	std::cout << "Input needed number of visits and press [ENTER]: ";
	std::cin >> numberOfVisits;

	FillRandomVisits ( *(pMap), numberOfVisits );

	if (_argc == 2) {
		std::ofstream resultFile(_argv[1]);
		PrintVisitDataByMaxAttandance (*(pMap), resultFile, '-' );
		resultFile << "\nAll data:";
		PrintVisitDataByAllPeriods (*(pMap), resultFile, '-');
		resultFile.close();
	}
	else {
		PrintVisitDataByMaxAttandance (*(pMap), std::cout, '-');
		std::cout << "\nAll data:";
		PrintVisitDataByAllPeriods (*(pMap), std::cout, '-');
	}

	TimeRangeMapDestroy (pMap);
}


