/*
 * customer_visit_time.hpp
 *
 *  Created on: May 4, 2015
 *      Author: betatester
 */

#ifndef CUSTOMER_VISIT_TIME_HPP_
#define CUSTOMER_VISIT_TIME_HPP_

#include <iostream>

const int HOURS_IN_DAY = 24, MINUTES_IN_HOUR = 60;

struct TimeRange
{
	short m_startHour, m_startMinute;
	short m_finishHour, m_finishMinute;
};

struct AbsoluteMinuteRange {
	int m_start, m_end;
};

bool IsProcessPresentInTimeRange (const TimeRange & _visitData, const AbsoluteMinuteRange _range);

void FillVisitPeriods (TimeRange & _visitData, const int _nOfPeriods);

void PrintVisitTime (TimeRange & _visitData, std::ostream & _stream, const char _sep = '\t');

void ConvertDayPeriodIDtoTime (const int _periodID, TimeRange & _time, const int _nOfPeriodsInDay);

#endif /* CUSTOMER_VISIT_TIME_HPP_ */
