/*
 * time_range_multimap.hpp
 *
 *  Created on: 6 мая 2015
 *      Author: strangeman
 */

#ifndef TIME_RANGE_MULTIMAP_HPP_
#define TIME_RANGE_MULTIMAP_HPP_

#include "time_range_vector.hpp"
#include "vector_of_time_range_vectors.hpp"

//Простейшая реализация, без использования вектора ключей

struct TimeRangeMap
{
    VOfTimeRangeVs m_values;
    int m_nOfPeriods;
    bool m_isEmpty = true;
};

TimeRangeMap * TimeRangeMapCreate (int _nOfPeriods);

void TimeRangeMapDestroy ( TimeRangeMap * _pMap );

void TimeRangeMapClear ( TimeRangeMap & _map );

bool TimeRangeMapIsEmpty ( const TimeRangeMap & _map );

bool TimeRangeMapHasKey ( const TimeRangeMap & _map, int _key );

int TimeRangeMapFindKeyPosition ( const TimeRangeMap & _map, int _key );

const TimeRangeVector & TimeRangeMapGet ( const TimeRangeMap & _map, int _key );

int TimeRangeMapCountKey ( const TimeRangeMap & _map, int _key );

void TimeRangeMapInsertKey ( TimeRangeMap & _map, int _key, const TimeRange & _value );

void TimeRangeMapRemoveKey ( TimeRangeMap & _map, int _key );



#endif /* TIME_RANGE_MULTIMAP_HPP_ */
