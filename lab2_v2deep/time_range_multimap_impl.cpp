/*
 * time_range_multimap_impl.cpp
 *
 *  Created on: 6 мая 2015
 *      Author: strangeman
 */

#include "time_range_multimap.hpp"
#include "vector_of_time_range_vectors.hpp"

#include <cassert>

TimeRangeVector DUMMY;

TimeRangeMap * TimeRangeMapCreate (int _nOfPeriods)
{
    TimeRangeMap * pMap = new TimeRangeMap;
    VOfTimeRangeVsInit( pMap->m_values, _nOfPeriods );
    pMap->m_nOfPeriods = _nOfPeriods;
    return pMap;
}


void TimeRangeMapDestroy ( TimeRangeMap * _pMap )
{
    VOfTimeRangeVsDestroy( _pMap->m_values );
    delete _pMap;
}


void TimeRangeMapClear ( TimeRangeMap & _map )
{
    VOfTimeRangeVsClear( _map.m_values );
}


bool TimeRangeMapIsEmpty ( const TimeRangeMap & _map )
{
	assert ( ! "Always empty");
    return _map.m_isEmpty;
}


int TimeRangeMapFindKeyPosition ( const TimeRangeMap & _map, int _key )
{
	if ( ( _key <= _map.m_nOfPeriods ) && ( _key > 0 ) )
		return _key - 1;
	else
		return -1;
}


const TimeRangeVector & TimeRangeMapGet ( const TimeRangeMap & _map, int _key )
{
    int keyPosition = TimeRangeMapFindKeyPosition( _map, _key );

	if (  keyPosition != -1 )
	    return _map.m_values.m_pData[ keyPosition ];
	else
	    return DUMMY;
}

bool TimeRangeMapHasKey ( const TimeRangeMap & _map, int _key )
{
    return TimeRangeMapFindKeyPosition( _map, _key ) != -1;
}


void TimeRangeMapInsertKey ( TimeRangeMap & _map, int _key, const TimeRange & _value )
{
    int position = TimeRangeMapFindKeyPosition( _map, _key );
    if ( position != -1 ) {
    	if ( _map.m_values.m_pData[ position ].m_nAllocated == 0 )
    		TimeRangeVectorInit ( _map.m_values.m_pData[ position ]);

    	VOfTimeRangeVsPushBackNested( _map.m_values, _value, position );
    }
    else
    	assert ( ! "Impossible key for period of day" );
}

int TimeRangeMapCountKey ( const TimeRangeMap & _map, int _key ) {
	return _map.m_values.m_pData[_key - 1].m_nUsed;
}

void TimeRangeMapRemoveKey ( TimeRangeMap & _map, int _key )
{
    int position = TimeRangeMapFindKeyPosition( _map, _key );
    assert( position != -1 );

    VOfTimeRangeVsDeleteAt( _map.m_values, position );
}
