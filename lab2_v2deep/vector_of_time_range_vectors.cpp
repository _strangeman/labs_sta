/*
 * vector_of_time_range_vectors.cpp
 *
 *  Created on: 6 мая 2015
 *      Author: strangeman
 */

#include "vector_of_time_range_vectors.hpp"
#include "cassert"
#include <cstring>
#include <iostream>

// VOfTimeRangeVs = vector of [time range vector]'s

void VOfTimeRangeVsInit ( VOfTimeRangeVs & _vector, int _allocatedSize ) {
    _vector.m_pData      = new TimeRangeVector[ _allocatedSize ];
    _vector.m_nAllocated =  _allocatedSize;
    _vector.m_nUsed      = 0;
}

void VOfTimeRangeVsDestroy ( VOfTimeRangeVs & _vector ) {
	for ( int i = 0; i < _vector.m_nAllocated; i++ ) {
		if ( _vector.m_pData[i].m_nAllocated == 0 )
			break;

		else
			TimeRangeVectorDestroy( _vector.m_pData[i] );
	}

	delete [] _vector.m_pData;
}

void VOfTimeRangeVsClear ( VOfTimeRangeVs & _vector ) {
	for ( int i = 0; i < _vector.m_nUsed; i++ )
		TimeRangeVectorClear( _vector.m_pData[i] );

	_vector.m_nUsed = 0;
}

bool VOfTimeRangeVsIsEmpty ( const VOfTimeRangeVs & _vector ) {
	return _vector.m_nUsed == 0;
}


void VOfTimeRangeVsGrow ( VOfTimeRangeVs & _vector )
{
    int nAllocatedNew = _vector.m_nAllocated * 2;
    TimeRangeVector * pNewData = new TimeRangeVector[ nAllocatedNew ];

    memcpy( pNewData, _vector.m_pData, sizeof( TimeRangeVector ) * _vector.m_nAllocated );

    delete[] _vector.m_pData;
    _vector.m_pData = pNewData;

    _vector.m_nAllocated = nAllocatedNew;
}

void VOfTimeRangeVsPushBack ( VOfTimeRangeVs & _vector, const TimeRangeVector & _data ) {
	int freeNestedIndex = _vector.m_nUsed++;
	TimeRangeVector freeNested = _vector.m_pData[freeNestedIndex];

	if ( freeNested.m_nAllocated == 0 )
		TimeRangeVectorInit( freeNested, _data.m_nAllocated );

	if ( freeNested.m_nAllocated < _data.m_nAllocated) {
		TimeRangeVectorDestroy( freeNested );
		TimeRangeVectorInit( freeNested, _data.m_nAllocated );
	}

	memcpy ( freeNested.m_pData, _data.m_pData, sizeof( TimeRange ) * _data.m_nAllocated );
	freeNested.m_nUsed = _data.m_nUsed;

	_vector.m_pData[freeNestedIndex] = freeNested;
}

void VOfTimeRangeVsPopBack ( VOfTimeRangeVs & _vector ) {
    assert( ! VOfTimeRangeVsIsEmpty( _vector ) );

    -- _vector.m_nUsed;
}

void VOfTimeRangeVsInsertAt ( VOfTimeRangeVs & _vector, int _position, const TimeRangeVector & _data ) {
    assert( _position >= 0 && _position < _vector.m_nUsed );

    int newUsed = _vector.m_nUsed + 1;
    if ( newUsed > _vector.m_nAllocated )
    	VOfTimeRangeVsGrow( _vector );

    TimeRangeVector freeNested = _vector.m_pData[_vector.m_nUsed];

    for ( int i = _vector.m_nUsed; i > _position; i-- )
        _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

	if ( freeNested.m_nAllocated == 0 )
		TimeRangeVectorInit( freeNested, _data.m_nAllocated );

	if ( freeNested.m_nAllocated < _data.m_nAllocated) {
		TimeRangeVectorDestroy( freeNested );
		TimeRangeVectorInit( freeNested, _data.m_nAllocated );
	}

	memcpy ( freeNested.m_pData, _data.m_pData, sizeof( TimeRange ) * _data.m_nAllocated );
	freeNested.m_nUsed = _data.m_nUsed;

	_vector.m_pData[ _position ] = freeNested;
}

void VOfTimeRangeVsDeleteAt ( VOfTimeRangeVs & _vector, int _position ) {
    assert( _position >= 0 && _position < _vector.m_nUsed );

    TimeRangeVector freeNested;
    freeNested = _vector.m_pData[ _position ];

    for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
        _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

    -- _vector.m_nUsed;

    _vector.m_pData[ _vector.m_nUsed ] = freeNested;
}

void VOfTimeRangeVsDestroyUnusedNested ( VOfTimeRangeVs & _vector ) {
	for (int i = _vector.m_nAllocated - 1; i >= _vector.m_nUsed; i--)
		if ( _vector.m_pData[i].m_nUsed == 0 )
			TimeRangeVectorDestroy(_vector.m_pData[i]);
}

void VOfTimeRangeVsDeleteAtNested ( VOfTimeRangeVs & _vector, int _indexOfNested, int _posInNested ) {
	assert ( ( _indexOfNested >= 0 ) && ( _indexOfNested <_vector.m_nUsed ) );

	if ( _posInNested == -1 )
		TimeRangeVectorDeleteAt( _vector.m_pData[ _indexOfNested ], _posInNested );

	else
		TimeRangeVectorPopBack( _vector.m_pData[ _indexOfNested ] );
}

void VOfTimeRangeVsInsertAtNested ( VOfTimeRangeVs & _vector, const TimeRange & _data, int _indexOfNested, int _posInNested ) {
	assert ( ( _indexOfNested >= 0 ) && ( _indexOfNested <_vector.m_nUsed ) );

	if ( _vector.m_pData[ _indexOfNested ].m_nAllocated == 0)
		TimeRangeVectorInit(_vector.m_pData[ _indexOfNested ]);

	TimeRangeVectorInsertAt( _vector.m_pData[ _indexOfNested ], _posInNested, _data );
}

void VOfTimeRangeVsPushBackNested ( VOfTimeRangeVs & _vector, const TimeRange & _data, int _indexOfNested ) {
	if ( _vector.m_pData[ _indexOfNested ].m_nAllocated == 0)
		TimeRangeVectorInit(_vector.m_pData[ _indexOfNested ]);

	TimeRangeVectorPushBack( _vector.m_pData[ _indexOfNested ], _data );
}

void VOfTimeRangeVsPopBackAtNested ( VOfTimeRangeVs & _vector, int _indexOfNested ) {
	assert ( ( _indexOfNested >= 0 ) && ( _indexOfNested <_vector.m_nUsed ) );

	TimeRangeVectorPopBack( _vector.m_pData[ _indexOfNested ] );
}

void VOfTimeRangeVsRead ( VOfTimeRangeVs & _vector, std::istream & _stream ) {
	assert ( ! "Not implemented yet" );
}

void VOfTimeRangeVsPrint ( const VOfTimeRangeVs & _vector, std::ostream & _stream, char _sep ) {
	assert ( ! "Not implemented yet" );
}
