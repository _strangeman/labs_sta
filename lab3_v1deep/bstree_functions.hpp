/*
 * bstree_functions.hpp
 *
 *  Created on: Jun 2, 2015
 *      Author: betatester
 */

#ifndef BSTREE_FUNCTIONS_HPP_
#define BSTREE_FUNCTIONS_HPP_

#include "bstree.hpp"
#include "cc_ptr_vector.hpp"

#include <iostream>

void BSTreeConvert( const BSTree & _tree, CCPtrVector & _vector );

void BSTreeRead( BSTree & _tree, std::istream & _stream );

void BSTreePrint( const BSTree & _tree, std::ostream & _stream );


#endif /* BSTREE_FUNCTIONS_HPP_ */
