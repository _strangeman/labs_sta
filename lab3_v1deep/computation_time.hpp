/*
 * computation_time.hpp
 *
 *  Created on: Jun 2, 2015
 *      Author: betatester
 */

#ifndef COMPUTATION_TIME_HPP_
#define COMPUTATION_TIME_HPP_

void FixComputationTime(double * _diff = nullptr);

#endif /* COMPUTATION_TIME_HPP_ */
