/*
 * lab3_v1deep.cpp
 *
 *  Created on: Jun 1, 2015
 *      Author: betatester
 */

#include "bstree.hpp"
#include "tree_realization_test.hpp"
#include "bstree_functions.hpp"
#include "computation_time.hpp"
#include <iostream>
#include <fstream>

int AnalyzeArguments( int _argc, char** _argv, std::ostream & _stream = std::cout ) {
	// Проверяем количество аргументов
	switch ( _argc ) {
	// Если не указано никаких аргументов - запускаем тест реализации
	case 1:
		_stream << "No arguments specified, testing of BSTree realization\n";
		if ( TreeRealizationTest() ) {
			_stream << "\nRealization of tree is incorrect\n";
			return -1;
		}

		else
			_stream << "\nRealization of tree is correct\n";
			return 1;

	// Если не хватает аргументов
	case 2:
		_stream << "You haven't specified the name of a result file\n"
				"\nExiting...\n";
		return -1;

	// Если аргументов хватает
	case 3:
		break;

	// Если аргументов слишком много
	default:
		_stream << "Warning: Using only first two arguments\n";
	}

	return 0;
}

bool AreFilesOpened( std::ifstream & _sourceFile, std::ofstream & _resultFile, std::ostream & _stream = std::cout ) {
	// Проверяем, удалось ли получить доступ к файлам и открыть их
	if ( ! _sourceFile.is_open() ) {
		std::cout << "Unable to open a source file\n"
				"\nExiting...\n";
		return false;
	}

	if ( ! _resultFile.is_open() ) {
		std::cout << "Unable to open or create a file with results\n"
				"\nExiting...\n";
		return false;
	}

	return true;
}

int main( int _argc, char** _argv ) {
	// Проверяем аргументы
	int argumentStatus = AnalyzeArguments( _argc, _argv, std::cout) ;
	if ( argumentStatus != 0 )
		return argumentStatus;

	// Пытаемся открыть файлы на чтение и запись
	std::ifstream sourceFile(_argv[1]);
	std::ofstream resultFile(_argv[2]);

	if ( ! AreFilesOpened( sourceFile, resultFile, std::cout ) )
		return -2;

	// Все в порядке, начинаем
	FixComputationTime();

	BSTree * tree = BSTreeCreate();

	std::cout << "\nReading... " << std::endl;

	BSTreeRead( *tree, sourceFile );

	double readTime;
	FixComputationTime( & readTime );
	std::cout << "Reading finished at " << readTime << "s \n";

	std::cout << "\nWriting... " << std::endl;
	BSTreePrint( *tree, resultFile );

	double writeTime;
	FixComputationTime( & writeTime );
	std::cout << "Writing finished at " << writeTime + readTime << "s \n";

	BSTreeDestroy( tree );

	double finishTime;
	FixComputationTime( & finishTime );

	std::cout << "\nProgram finished in " << writeTime + readTime + finishTime<< " seconds\n";
	return 0;
}
